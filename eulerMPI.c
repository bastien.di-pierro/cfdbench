#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <mpi.h>

#define GAMMA 1.4
int decoupe(int iCPU, int NCPU, int N, double dx, double *x0)
{
  int NP, reste;
  NP = N / NCPU;      
  reste = N % NCPU;  
  if(iCPU < reste)
  {   
    NP += 1;          
    *x0 = (double)(NP*iCPU)*dx;
  }
  else
  {               
    *x0 = (double)((NP+1)*reste + NP*(iCPU-reste) )*dx;
  }
  return NP; 
}

void echange(int N, int myID, int NCPU, double *cons)
{

  int iGauche = myID - 1;
  int iDroite = myID + 1;
  MPI_Status statut;
  int ierr;

  if(myID>0) 
  {   
    if(myID%2==0) 
    {  
      ierr = MPI_Send(&cons[3],3,MPI_DOUBLE_PRECISION,iGauche,iGauche,MPI_COMM_WORLD);
      ierr = MPI_Recv(&cons[0],3,MPI_DOUBLE_PRECISION,iGauche, myID, MPI_COMM_WORLD,&statut);
    }
    else
    {
      ierr = MPI_Recv(&cons[0],3,MPI_DOUBLE_PRECISION,iGauche, myID, MPI_COMM_WORLD,&statut);
      ierr = MPI_Send(&cons[3],3,MPI_DOUBLE_PRECISION,iGauche,iGauche,MPI_COMM_WORLD);
    }
  }
  if(myID<NCPU-1)
  {
    if(myID%2==1)
    {
      ierr = MPI_Recv(&cons[3*(N+1)],3,MPI_DOUBLE_PRECISION,iDroite,myID,MPI_COMM_WORLD,&statut);
      ierr = MPI_Send(&cons[3*N    ],  3,MPI_DOUBLE_PRECISION,iDroite,iDroite,MPI_COMM_WORLD);
    }
    else
    {
      ierr = MPI_Send(&cons[3*N],    3,MPI_DOUBLE_PRECISION,iDroite,iDroite,MPI_COMM_WORLD);
      ierr = MPI_Recv(&cons[3*(N+1)],3,MPI_DOUBLE_PRECISION,iDroite,myID,MPI_COMM_WORLD,&statut);
    }
  }
}

double proe(double ro,double e,double gama)
{
  return (gama - 1.0)*ro*e;
}

double erop(double ro, double p, double gama)
{
  return p/(ro*(gama-1.0));
}

double crop(double ro, double p, double gama)
{
  return pow(gama*p/ro,0.5);
}

void constoprim(double *cons, double *prim, int N)
{
  double e;
  int i;
  double gama=GAMMA;
  for(i=0;i<=N+1;i++)
  {
    prim[i*3  ] = cons[3*i];
    prim[i*3+1] = cons[3*i+1]/cons[3*i];
    e           = cons[3*i+2]/cons[3*i] - 0.5*prim[3*i+1]*prim[3*i+1];
    prim[3*i+2] = proe(prim[3*i],e,gama);
  }
}

void integre(double *cons, double *flux, int N, double dt, double dx)
{
  int i;
  for(i=1;i<=N;i++)
  {
    cons[3*i  ] = cons[3*i  ] - dt/dx * (flux[3*i  ] - flux[3*(i-1)  ]);
    cons[3*i+1] = cons[3*i+1] - dt/dx * (flux[3*i+1] - flux[3*(i-1)+1]);
    cons[3*i+2] = cons[3*i+2] - dt/dx * (flux[3*i+2] - flux[3*(i-1)+2]);
  }
}

void hllc(double ror,double ur,double pr,double er,double ar,double rol,double ul,double pl,double el,double al,double *Fmas,double *Fqdm,double *Fnrj,double *ustar,double *cmax) 
{
  double uud[3], uug[3], fr[3],fl[3],qq[3],zz[3],var[3];
  double  sr1,sr2,sr,sl1,sl2,sl,sm,rostarl,rostarr,pstarl,pstarr,estarr,estarl;
  int i;
  double gama=GAMMA;
  
  sr1  = ur+ar ; 
  sr2  = ur-ar ; 
  sl1  = ul+al ; 
  sl2  = ul-al ;
  sl   = fmin(sr2,sl2) ; 
  sr   = fmax(sr1,sl1) ;
  *cmax = fmax(fabs(sl),fabs(sr)) ;

  uud[0] = ror                  ; 
  uud[1] = ror*ur               ; 
  uud[2] = ror*(er+0.5*ur*ur)   ; 
  uug[0] = rol                  ; 
  uug[1] = rol*ul               ; 
  uug[2] = rol*(el+0.5*ul*ul)   ; 
  fr[0]  = ror*ur               ;
  fr[1]  = ror*ur*ur + pr       ;
  fr[2]  = ur*(uud[2] + pr)     ;
  fl[0]  = rol*ul               ;
  fl[1]  = rol*ul*ul + pl       ;
  fl[2]  = ul*(uug[2] + pl)     ;
  
  for(i=0;i<3;i++)
  {
    var[i] = (sr*uud[i] - sl*uug[i] - (fr[i] - fl[i]) )/(sr - sl);
  }
  sm = var[1]/var[0];
  *ustar = sm;

  for(i=0;i<3;i++)
  {
    qq[i] = sl*uug[i] - fl[i];
  }
  rostarl = qq[0]/(sl-sm); 
  pstarl  = sm*qq[0]-qq[1]; 
  estarl  = (sm*pstarl+qq[2])/(sl-sm);

  for(i=0;i<3;i++)
  {
    zz[i] = sr*uud[i] - fr[i];
  }  
  rostarr = zz[0]/(sr-sm); 
  pstarr  = sm*zz[0]-zz[1]; 
  estarr  = (sm*pstarr+zz[2])/(sr-sm);
  
  if(sl>0.0)
  {
    *Fmas = fl[0];
    *Fqdm = fl[1];
    *Fnrj = fl[2];
  }
  else if(sr<0.0)
  {
    *Fmas = fr[0];
    *Fqdm = fr[1];
    *Fnrj = fr[2];
  }
  else if(*ustar>0.0)
  {
    *Fmas = rostarl**ustar;
    *Fqdm = rostarl**ustar**ustar+pstarl;
    *Fnrj = *ustar*(estarl+pstarl);
  }
  else if(*ustar<0.0)
  {
    *Fmas = rostarr**ustar;
    *Fqdm = rostarr**ustar**ustar+pstarr;
    *Fnrj = *ustar*(estarr+pstarr);
  }
  else
  {
    *Fmas = 0.0 ;
    *Fqdm = pstarr; 
    *Fnrj = 0.0 ;
  }
}

void pbRiemann(int myID, int NCPU,double *prim, double *flux, double *cmax, int N)
{
  int i;
  double rol,ul,pl,el,al,ror,ur,pr,er,ar, Fmas, Fqdm,Fnrj,ustar,ccmax;
  double gama=GAMMA;

  if(myID==0)
  {
    for(i=0;i<3;i++)
    {
      prim[0+i]   = prim[3+i];
    }
  }
  if(myID==NCPU-1)
  {
    for(i=0;i<3;i++)
    {
    prim[3*(N+1)+i] = prim[3*N+i];
    }
  }
  
  *cmax = 0.0;
  for(i=0;i<N+1;i++)
  {
    rol = prim[3*i]   ; 
    ul  = prim[3*i+1]   ; 
    pl  = prim[3*i+2]   ; 
    el  = erop(rol,pl,gama) ; 
    al  = crop(rol,pl,gama) ;
    ror = prim[3*(i+1)] ; 
    ur  = prim[3*(i+1)+1] ; 
    pr  = prim[3*(i+1)+2] ; 
    er  = erop(ror,pr,gama) ;
    ar  = crop(ror,pr,gama) ;
    hllc(ror,ur,pr,er,ar,rol,ul,pl,el,al,&Fmas,&Fqdm,&Fnrj,&ustar,&ccmax);
    *cmax = fmax(*cmax,ccmax);
    flux[3*i  ] = Fmas;
    flux[3*i+1] = Fqdm;
    flux[3*i+2] = Fnrj;
  }
  MPI_Allreduce(cmax,cmax,1,MPI_DOUBLE_PRECISION,MPI_MAX,MPI_COMM_WORLD);
}

void ecrit(int myID,int NCPU,int N,FILE* fichier,double *x, double *prim)
{
  int i,icpu;
  for(icpu=0;icpu<NCPU;icpu++)
  {
    if(myID==icpu)
    {
      if(myID==0)
      {
        fichier = fopen("eulerc.dat","w");
      }
      else
      {
        fichier = fopen("eulerc.dat","a");
      }
      for(i=1;i<=N;i++)
      {
        fprintf(fichier,"%.16lf %.16lf %.16lf %.16lf\n",x[i],prim[3*i], prim[3*i+1], prim[3*i+2]);
      }
      fclose(fichier); // fermeture
    }
    MPI_Barrier(MPI_COMM_WORLD);
  }
}

void init(int N, double x0, double dx, double *x, double *prim, double *cons)
{
  double gama=GAMMA;
  int i;
  

  for(i=0;i<=N+1;i++)
  {
    x[i] = x0 + ((double) i - 0.5)*dx;
    if(x[i]<1)
    {
      prim[3*i  ] = 1.0;
      prim[3*i+1] = 0.0;
      prim[3*i+2] = 1.0;
    }
    else
    {
      prim[3*i  ] = 0.125;
      prim[3*i+1] = 0.0;
      prim[3*i+2] = 0.1;
    }
    cons[3*i  ] = prim[3*i];
    cons[3*i+1] = prim[3*i]*prim[3*i+1];
    cons[3*i+2] = prim[3*i]*(erop(prim[3*i],prim[3*i+2],gama)+0.5*prim[3*i+1]*prim[3*i+1]);
  }
}

int main(int argc, char *argv[])
{
  int N, Nt,i,it;
  FILE* param ;
  FILE* out  ; 
  double *x, *prim, *cons,*flux;
  double cmax,dx,dt,t=0.0;
  int Np,NCPU,myID;
  double x0;
  clock_t deb,fin;
  double Ttotal;

  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD,&NCPU);
  MPI_Comm_rank(MPI_COMM_WORLD,&myID);

  if(myID==0)
  {
    param = fopen("param.dat","r");
    fscanf(param,"%d",&N);
    fscanf(param,"%d",&Nt);
  }
  MPI_Bcast(&N ,1,MPI_INT,0,MPI_COMM_WORLD);
  MPI_Bcast(&Nt,1,MPI_INT,0,MPI_COMM_WORLD);
  dx = 2.0/(double) N;
  dt = 0.0000000001;

  Np = decoupe(myID,NCPU,N,dx,&x0);
  x  = malloc((Np+2)*sizeof(double));
  cmax = 0.0;
  prim = malloc(3*(Np+2)*sizeof(double));
  cons = malloc(3*(Np+2)*sizeof(double));
  flux = malloc(3*(Np+2)*sizeof(double));

  init(Np,x0,dx,x,prim,cons);

  deb = clock();

  for(it=1;it<=Nt;it++)
  {
    echange(Np,myID,NCPU,cons);
    constoprim(cons,prim,Np);
    pbRiemann(myID,NCPU,prim,flux,&cmax,Np); 
    dt = 0.5*dx/cmax;
    t = t + dt;
    integre(cons,flux,Np,dt,dx);
  }
  fin = clock();
  Ttotal = (double) (fin - deb)/ CLOCKS_PER_SEC;
  if(myID==0)
  {
    printf("MPI %d CPU : %lf s \n",NCPU,Ttotal);
    printf("temps final :%lf \n",t);
  }
  constoprim(cons,prim,Np);
  ecrit(myID,NCPU,Np,out,x,prim);
  free(x);
  free(cons);
  free(prim);
  free(flux);
  MPI_Finalize();
  return 0;
}
