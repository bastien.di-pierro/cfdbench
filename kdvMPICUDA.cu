#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>

#define MAX(a,b) (((a)>(b))?(a):(b))

#define Nth 16
#define Nbl (N+2)/Nth+1

int decoupe(int iCPU, int NCPU, int N, double dx, double *x0)
{
  int NP, reste;
  NP = N / NCPU;   
  reste = N % NCPU;
  if(iCPU < reste)
  {   
    NP += 1; 
    *x0 = (double)(NP*iCPU)*dx;
  }
  else
  {         
    *x0 = (double)((NP+1)*reste + NP*(iCPU-reste) )*dx;
  }
  return NP+4;
}

void echange(int N, int myID, int Ncpu, double *phi)
{
  int iGauche = myID - 1;
  int iDroite = myID + 1;
  MPI_Status statut;
  int ierr;
  if(myID == 0) iGauche = Ncpu - 1; 
  if(myID == Ncpu - 1) iDroite = 0;

  if(myID%2==0) 
  { 
    MPI_Send(&phi[2],1,MPI_DOUBLE_PRECISION,iGauche,iGauche,MPI_COMM_WORLD);
    MPI_Send(&phi[3],1,MPI_DOUBLE_PRECISION,iGauche,1000+iGauche,MPI_COMM_WORLD);
    MPI_Recv(&phi[0],1,MPI_DOUBLE_PRECISION,iGauche,2000+myID,MPI_COMM_WORLD,&statut);
    MPI_Recv(&phi[1],1,MPI_DOUBLE_PRECISION,iGauche,3000+myID,MPI_COMM_WORLD,&statut);
    MPI_Send(&phi[N-4],1,MPI_DOUBLE_PRECISION,iDroite,2000+iDroite,MPI_COMM_WORLD);
    MPI_Send(&phi[N-3],1,MPI_DOUBLE_PRECISION,iDroite,3000+iDroite,MPI_COMM_WORLD);
    MPI_Recv(&phi[N-2],1,MPI_DOUBLE_PRECISION, iDroite,myID,MPI_COMM_WORLD,&statut);
    MPI_Recv(&phi[N-1],1,MPI_DOUBLE_PRECISION, iDroite,myID+1000,MPI_COMM_WORLD,&statut);
  }
  else
  {  
    MPI_Recv(&phi[N-2],1,MPI_DOUBLE_PRECISION, iDroite,myID,MPI_COMM_WORLD,&statut);
    MPI_Recv(&phi[N-1],1,MPI_DOUBLE_PRECISION, iDroite,myID+1000,MPI_COMM_WORLD,&statut);
    MPI_Send(&phi[N-4],1,MPI_DOUBLE_PRECISION,iDroite,2000+iDroite,MPI_COMM_WORLD);
    MPI_Send(&phi[N-3],1,MPI_DOUBLE_PRECISION,iDroite,3000+iDroite,MPI_COMM_WORLD);
    MPI_Recv(&phi[0],1,MPI_DOUBLE_PRECISION,iGauche,2000+myID,MPI_COMM_WORLD,&statut);
    MPI_Recv(&phi[1],1,MPI_DOUBLE_PRECISION,iGauche,3000+myID,MPI_COMM_WORLD,&statut);
    MPI_Send(&phi[2],1,MPI_DOUBLE_PRECISION,iGauche,iGauche,MPI_COMM_WORLD);
    MPI_Send(&phi[3],1,MPI_DOUBLE_PRECISION,iGauche,1000+iGauche,MPI_COMM_WORLD);
  }
}

void init(int N, double x0, double dx, double *x, double *phi)
{
  int i;
  for(i=0; i<N;i++)
  {
    x[i] = x0 + (double)i*dx;
    phi[i] = 0.5/(cosh(0.5*x[i])*cosh(0.5*x[i]));
  }
}

void ecrit(int myID, int NCPU, int N, double *x, double *phi,int cas)
{
  int i;
  int icpu;
  FILE* fichier;
  for(icpu=0;icpu<NCPU;icpu ++)
  {
    if(icpu==myID)
    {
      if(cas == 0)
      {
        if(myID==0)
        {
          fichier = fopen("initial.dat","w"); 
        }
        else
        {
          fichier = fopen("initial.dat","a");
        }
      }
      else
      {
        if(myID==0)
        {
          fichier = fopen("finale.dat","w");
        }
        else
        {
          fichier = fopen("finale.dat","a"); 
        }
      }
      for(i=2;i<N-2;i++)
      {
        fprintf(fichier,"%lf %lf \n",x[i],phi[i]);
      }
      fclose(fichier); 
    }
    MPI_Barrier(MPI_COMM_WORLD);
  }

}

void copy(int N,double *phi1, double *phi2)
{
  int i;
  
  for(i=0;i<N;i++)
  {
    phi1[i] = phi2[i];
  }
}

//=====================================================================
__device__ __forceinline__ double atomMax(double *address, double val)
{
    unsigned long long ret = __double_as_longlong(*address);
    while(val > __longlong_as_double(ret))
    {
        unsigned long long old = ret;
        if((ret = atomicCAS((unsigned long long *)address, old, __double_as_longlong(val))) == old)
            break;
    }
    return __longlong_as_double(ret);
}

__device__ void redMaxDblOptDev(int N,double *a, double *maxi)
{
  __shared__ double tmp[Nth];
  int idx = blockIdx.x*blockDim.x + threadIdx.x,s;
 
  if(idx<N)
  {
    tmp[threadIdx.x] = a[idx];
  }
  else
  {
    tmp[threadIdx.x] = 0.;
  }

  __syncthreads();

  for(s=blockDim.x/2;s>0;s>>=1)
  {
    if(threadIdx.x<s)
    {
      tmp[threadIdx.x] = MAX(tmp[threadIdx.x],tmp[threadIdx.x+s]);
    }
    __syncthreads();
  }

  if(threadIdx.x==0)
  {
    atomMax(maxi,tmp[0]);
  }
}

__global__ void redMaxDblOptKer(int N,double *a, double *maxi)
{
  redMaxDblOptDev(N,a,maxi);
}

__device__ void smbKDV(int N, double dx,double a, double nu, double *phi, double *smb, double *amax)
{
  int idx = blockIdx.x*blockDim.x + threadIdx.x;
  double sdx = 1./dx ;
  
  if(idx>=2 and idx <=N-3)
  {
    smb[idx]  = -2.0*sdx*(phi[idx+1]+phi[idx]+phi[idx-1])*(phi[idx+1]-phi[idx-1]) - nu*sdx*sdx*sdx*(phi[idx+2] - 2.0*phi[idx+1] + 2.0*phi[idx-1] - phi[idx-2]);
    amax[idx] = fabs(phi[idx])+4.0*nu*sdx*sdx;
    printf("%d %lf \n",idx,amax[idx]);
  }
  else if(idx>=0 and idx<=N-1)
  {
    smb[idx]  = 0.0;
    amax[idx] = 0.0;
  }
}

__device__ void leap_frog(int N,  double dt, double *phi, double *phim, double *smb)
{
  int idx =  blockIdx.x*blockDim.x + threadIdx.x;
  double phisave;
  if(idx>=2 and idx<=N-3)
  {
    phisave   = phi[idx];
    phi[idx]  = phim[idx] + dt*smb[idx];
    phim[idx] = phisave; 
  }
}

__global__ void DFsmbKer(int N, double dx,double a, double nu, double *phi, double *smb, double *cm, double *cmax)
{
  smbKDV(N, dx,a, nu, phi, smb, cm);
  redMaxDblOptDev(N,cm,cmax);
}

__global__ void DFLFKer(int N, double dx,double *phi, double *phim, double *smb,double *cmax)
{
  double dt = 0.9*dx/ *cmax;
  leap_frog(N,dt,phi,phim,smb);
}


//=======================================================================================

int main(int argc, char* argv[])
{
  int N, Nt;
  double *x,*phi, *K1,*phim1,L,*cmax;
  double dx,a= 1.0,nu=1.000;
  FILE* param;
  int NCPU,myID;
  int Np;
  double x0;
  double *d_phi,*d_K1,*d_phim1,*d_cmax,*d_vmax;

  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD,&NCPU);
  MPI_Comm_rank(MPI_COMM_WORLD,&myID);

  if(myID == 0)
  {
    param = fopen("param.dat","r+");
    fscanf(param,"%d",&N);
    fscanf(param,"%d",&Nt);
  }
  MPI_Bcast(&N ,1,MPI_INT,0,MPI_COMM_WORLD);
  MPI_Bcast(&Nt,1,MPI_INT,0,MPI_COMM_WORLD);

  L = 10.0*4.0*atan(1.0); 

  dx = L/(double) (N-1);

  Np = decoupe(myID, NCPU, N-4, dx, &x0);

  x     = (double*)malloc(Np*sizeof(double));
  phi   = (double*)malloc(Np*sizeof(double));
  K1    = (double*)malloc(Np*sizeof(double));
  phim1 = (double*)malloc(Np*sizeof(double));
  cmax  = (double*)malloc(Np*sizeof(double));
  cudaMalloc((void**)&d_phi  ,Np*sizeof(double));
  cudaMalloc((void**)&d_K1   ,Np*sizeof(double));
  cudaMalloc((void**)&d_phim1,Np*sizeof(double));
  cudaMalloc((void**)&d_cmax ,Np*sizeof(double));
  cudaMalloc((void**)&d_vmax ,   sizeof(double));
 
  init(Np,-L/2.0+x0,dx,x,phi);
  ecrit(myID,NCPU,Np,x,phi,0);
  copy(Np,phim1,phi); 
  cudaMemcpy(d_phi  ,phi  ,Np*sizeof(double),cudaMemcpyHostToDevice);
  cudaMemcpy(d_K1   ,K1   ,Np*sizeof(double),cudaMemcpyHostToDevice);
  cudaMemcpy(d_phim1,phim1,Np*sizeof(double),cudaMemcpyHostToDevice);
  cudaMemcpy(d_cmax ,cmax ,Np*sizeof(double),cudaMemcpyHostToDevice);
  for(int i=0;i<Nt;i++)
  {
    echange(Np, myID, NCPU, d_phi);
    DFsmbKer<<<(Np+2)/Nth+1,Nth>>>(Np,dx,a,nu,d_phi,d_K1,d_cmax,d_vmax);
    cudaDeviceSynchronize();
    MPI_Allreduce(d_vmax,d_vmax,1,MPI_DOUBLE_PRECISION,MPI_MAX,MPI_COMM_WORLD);
    DFLFKer<<<(Np+2)/Nth+1,Nth>>>(Np,dx,d_phi,d_phim1,d_K1,d_vmax);
    cudaDeviceSynchronize();
  }
  cudaMemcpy(phi  ,d_phi,Np*sizeof(double),cudaMemcpyDeviceToHost);
  ecrit(myID,NCPU,Np,x,phi,1);
  cudaFree(d_phi);
  cudaFree(d_K1);
  cudaFree(d_phim1);
  cudaFree(d_cmax);
  cudaFree(d_vmax);
  free(x);
  free(phi);
  free(K1);
  free(phim1);
  free(cmax);
  MPI_Finalize();
  
  return 0;
}
  
