#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <mpi.h>

#define GAMMA 1.4
#define MAX(a,b) (((a)>(b))?(a):(b))

#define Nth 16
#define Nbl (N+2)/Nth+1

int decoupe(int iCPU, int NCPU, int N, double dx, double *x0)
{
  int NP, reste;
  NP = N / NCPU;     
  reste = N % NCPU; 
  if(iCPU < reste)
  {   
    NP += 1;      
    *x0 = (double)(NP*iCPU)*dx;
  }
  else
  {              
    *x0 = (double)((NP+1)*reste + NP*(iCPU-reste) )*dx;
  }
  return NP; 
}

void echange(int N, int myID, int NCPU, double *cons)
{

  int iGauche = myID - 1;
  int iDroite = myID + 1;
  MPI_Status statut;
  int ierr;

  if(myID>0)
  {   
    if(myID%2==0)
    {  
      ierr = MPI_Send(&cons[3],3,MPI_DOUBLE_PRECISION,iGauche,iGauche,MPI_COMM_WORLD);
      ierr = MPI_Recv(&cons[0],3,MPI_DOUBLE_PRECISION,iGauche, myID, MPI_COMM_WORLD,&statut);
    }
    else
    {
      ierr = MPI_Recv(&cons[0],3,MPI_DOUBLE_PRECISION,iGauche, myID, MPI_COMM_WORLD,&statut);
      ierr = MPI_Send(&cons[3],3,MPI_DOUBLE_PRECISION,iGauche,iGauche,MPI_COMM_WORLD);
    }
  }
  if(myID<NCPU-1)
  {
    if(myID%2==1)
    {
      ierr = MPI_Recv(&cons[3*(N+1)],3,MPI_DOUBLE_PRECISION,iDroite,myID,MPI_COMM_WORLD,&statut);
      ierr = MPI_Send(&cons[3*N    ],  3,MPI_DOUBLE_PRECISION,iDroite,iDroite,MPI_COMM_WORLD);
    }
    else
    {
      ierr = MPI_Send(&cons[3*N],    3,MPI_DOUBLE_PRECISION,iDroite,iDroite,MPI_COMM_WORLD);
      ierr = MPI_Recv(&cons[3*(N+1)],3,MPI_DOUBLE_PRECISION,iDroite,myID,MPI_COMM_WORLD,&statut);
    }
  }
}

void ecrit(int myID,int NCPU,int N,FILE* fichier,double *x, double *prim)
{
  int i,icpu;
  for(icpu=0;icpu<NCPU;icpu++)
  {
    if(myID==icpu)
    {
      if(myID==0)
      {
        fichier = fopen("eulerc.dat","w");
      }
      else
      {
        fichier = fopen("eulerc.dat","a");
      }
      for(i=1;i<=N;i++)
      {
        fprintf(fichier,"%.16lf %.16lf %.16lf %.16lf\n",x[i],prim[3*i], prim[3*i+1], prim[3*i+2]);
      }
      fclose(fichier); // fermeture
    }
    MPI_Barrier(MPI_COMM_WORLD);
  }
}

double Herop(double ro, double p, double gama)
{
  return p/(ro*(gama-1.0));
}

void init(int N, double x0, double dx, double *x, double *prim, double *cons)
{
  double gama=GAMMA;
  int i;
  

  for(i=0;i<=N+1;i++)
  {
    x[i] = x0 + ((double) i - 0.5)*dx;
    if(x[i]<1)
    {
      prim[3*i  ] = 1.0;
      prim[3*i+1] = 0.0;
      prim[3*i+2] = 1.0;
    }
    else
    {
      prim[3*i  ] = 0.125;
      prim[3*i+1] = 0.0;
      prim[3*i+2] = 0.1;
    }
    cons[3*i  ] = prim[3*i];
    cons[3*i+1] = prim[3*i]*prim[3*i+1];
    cons[3*i+2] = prim[3*i]*(Herop(prim[3*i],prim[3*i+2],gama)+0.5*prim[3*i+1]*prim[3*i+1]);
  }
}

double Hproe(double ro,double e,double gama)
{
  return (gama - 1.0)*ro*e;
}

void Hconstoprim(double *cons, double *prim, int N)
{
  double e;
  double gama=GAMMA;
  int i;
  for(i=0;i<=N+1;i++)
  {
    prim[3*i  ] = cons[3*i];
    prim[3*i+1] = cons[3*i+1]/cons[3*i];
    e           = cons[3*i+2]/cons[3*i] - 0.5*prim[3*i+1]*prim[3*i+1];
    prim[3*i+2] = Hproe(prim[3*i],e,gama);
  }
}


//=====================================================================
__device__ __forceinline__ double atomMax(double *address, double val)
{
    unsigned long long ret = __double_as_longlong(*address);
    while(val > __longlong_as_double(ret))
    {
        unsigned long long old = ret;
        if((ret = atomicCAS((unsigned long long *)address, old, __double_as_longlong(val))) == old)
            break;
    }
    return __longlong_as_double(ret);
}

__device__ void redMaxDblOptDev(int N,double *a, double *maxi)
{
  __shared__ double tmp[Nth];
  int idx = blockIdx.x*blockDim.x + threadIdx.x,s;
 
  if(idx<N)
  {
    tmp[threadIdx.x] = a[idx];
  }
  else
  {
    tmp[threadIdx.x] = 0.;
  }

  __syncthreads();

  for(s=blockDim.x/2;s>0;s>>=1)
  {
    if(threadIdx.x<s)
    {
      tmp[threadIdx.x] = MAX(tmp[threadIdx.x],tmp[threadIdx.x+s]);
    }
    __syncthreads();
  }

  if(threadIdx.x==0)
  {
    atomMax(maxi,tmp[0]);
  }
}

__global__ void redMaxDblOptKer(int N,double *a, double *maxi)
{
  redMaxDblOptDev(N,a,maxi);
}
__device__ double proe(double ro,double e,double gama)
{
  return (gama - 1.0)*ro*e;
}

__device__ double erop(double ro, double p, double gama)
{
  return p/(ro*(gama-1.0));
}

__device__ double crop(double ro, double p, double gama)
{
  return pow(gama*p/ro,0.5);
}

__device__ void constoprim(double *cons, double *prim, int N)
{
  double e;
  double gama=GAMMA;
  int idx = blockIdx.x*blockDim.x + threadIdx.x;
  if(idx>=0 and idx <=N+1)
  {
    prim[3*idx  ] = cons[3*idx];
    prim[3*idx+1] = cons[3*idx+1]/cons[3*idx];
    e           = cons[3*idx+2]/cons[3*idx] - 0.5*prim[3*idx+1]*prim[3*idx+1];
    prim[3*idx+2] = proe(prim[3*idx],e,gama);
  }
}

__device__ void integre(double *cons, double *flux, int N, double dt, double dx)
{
  int idx = blockIdx.x*blockDim.x + threadIdx.x;
  if(idx>=1 and idx<=N)
  {
    cons[3*idx  ] = cons[3*idx  ] - dt/dx * (flux[3*idx  ] - flux[3*(idx-1)  ]);
    cons[3*idx+1] = cons[3*idx+1] - dt/dx * (flux[3*idx+1] - flux[3*(idx-1)+1]);
    cons[3*idx+2] = cons[3*idx+2] - dt/dx * (flux[3*idx+2] - flux[3*(idx-1)+2]);
  }
}

__device__ void hllc(double ror,double ur,double pr,double er,double ar,double rol,double ul,double pl,double el,double al,double *Fmas,double *Fqdm,double *Fnrj,double *ustar,double *cmax) 
{
  double uud[3], uug[3], fr[3],fl[3],qq[3],zz[3],var[3];
  double  sr1,sr2,sr,sl1,sl2,sl,sm,rostarl,rostarr,pstarl,pstarr,estarr,estarl;
  int i;
  
  sr1  = ur+ar ; 
  sr2  = ur-ar ; 
  sl1  = ul+al ; 
  sl2  = ul-al ;
  sl   = fmin(sr2,sl2) ; 
  sr   = fmax(sr1,sl1) ;
  *cmax = fmax(fabs(sl),fabs(sr)) ;

  uud[0] = ror                  ; 
  uud[1] = ror*ur               ; 
  uud[2] = ror*(er+0.5*ur*ur)   ; 
  uug[0] = rol                  ; 
  uug[1] = rol*ul               ; 
  uug[2] = rol*(el+0.5*ul*ul)   ; 
  fr[0]  = ror*ur               ;
  fr[1]  = ror*ur*ur + pr       ;
  fr[2]  = ur*(uud[2] + pr)     ;
  fl[0]  = rol*ul               ;
  fl[1]  = rol*ul*ul + pl       ;
  fl[2]  = ul*(uug[2] + pl)     ;
  
  for(i=0;i<3;i++)
  {
    var[i] = (sr*uud[i] - sl*uug[i] - (fr[i] - fl[i]) )/(sr - sl);
  }
  sm = var[1]/var[0];
  *ustar = sm;

  for(i=0;i<3;i++)
  {
    qq[i] = sl*uug[i] - fl[i];
  }
  rostarl = qq[0]/(sl-sm); 
  pstarl  = sm*qq[0]-qq[1]; 
  estarl  = (sm*pstarl+qq[2])/(sl-sm);

  for(i=0;i<3;i++)
  {
    zz[i] = sr*uud[i] - fr[i];
  }  
  rostarr = zz[0]/(sr-sm); 
  pstarr  = sm*zz[0]-zz[1]; 
  estarr  = (sm*pstarr+zz[2])/(sr-sm);
  
  if(sl>0.0)
  {
    *Fmas = fl[0];
    *Fqdm = fl[1];
    *Fnrj = fl[2];
  }
  else if(sr<0.0)
  {
    *Fmas = fr[0];
    *Fqdm = fr[1];
    *Fnrj = fr[2];
  }
  else if(*ustar>0.0)
  {
    *Fmas = rostarl**ustar;
    *Fqdm = rostarl**ustar**ustar+pstarl;
    *Fnrj = *ustar*(estarl+pstarl);
  }
  else if(*ustar<0.0)
  {
    *Fmas = rostarr**ustar;
    *Fqdm = rostarr**ustar**ustar+pstarr;
    *Fnrj = *ustar*(estarr+pstarr);
  }
  else
  {
    *Fmas = 0.0 ;
    *Fqdm = pstarr; 
    *Fnrj = 0.0 ;
  }
}

__device__ void pbRiemann(int myID, int NCPU,double *prim, double *flux, double *cmax, int N)
{
  int i;
  double rol,ul,pl,el,al,ror,ur,pr,er,ar, Fmas, Fqdm,Fnrj,ustar,ccmax;
  double gama=GAMMA;
  int idx = blockIdx.x*blockDim.x + threadIdx.x;

  if(myID==0 and idx==0)
  {
    for(i=0;i<3;i++)
    {
      prim[0+i]   = prim[3+i];
    }
  }
  if(myID==NCPU-1 and idx==N+1)
  {
    for(i=0;i<3;i++)
    {
    prim[3*(N+1)+i] = prim[3*N+i];
    }
  }
  
  cmax[idx] = 0.0;
  if(idx>=0 and idx<=N+1)
  {
    rol = prim[3*idx]   ; 
    ul  = prim[3*idx+1]   ; 
    pl  = prim[3*idx+2]   ; 
    el  = erop(rol,pl,gama) ; 
    al  = crop(rol,pl,gama) ;
    ror = prim[3*(idx+1)] ; 
    ur  = prim[3*(idx+1)+1] ; 
    pr  = prim[3*(idx+1)+2] ; 
    er  = erop(ror,pr,gama) ;
    ar  = crop(ror,pr,gama) ;
    hllc(ror,ur,pr,er,ar,rol,ul,pl,el,al,&Fmas,&Fqdm,&Fnrj,&ustar,&ccmax);
    cmax[idx] = fmax(cmax[idx],ccmax);
    flux[3*idx  ] = Fmas;
    flux[3*idx+1] = Fqdm;
    flux[3*idx+2] = Fnrj;
  }
}

__global__ void VFHLLKer(int myID, int NCPU, int N, double dx, double *prim, double *cons, double *flux, double *cm, double *cmax)
{
  constoprim(cons,prim,N);
  pbRiemann(myID,NCPU,prim,flux,cm,N); 
  redMaxDblOptDev(N,cm,cmax);
}

__global__ void VFINTKer(int N, double dx, double *cons, double *flux, double *cmax)
{
  double dt = 0.5*dx/ *cmax;
  integre(cons,flux,N,dt,dx);
}

//==============================================
int main(int argc, char *argv[])
{
  int N, Nt,it;
  FILE* param ;
  FILE* out  ; 
  double *x, *prim, *cons,*flux;
  double dx;
  int Np,NCPU,myID,gpuID;
  double x0;
  clock_t deb,fin;
  double Ttotal;
  double *d_prim, *d_cons, *d_flux, *d_cmax, *d_vmax;
  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);
  float tms;

  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD,&NCPU);
  MPI_Comm_rank(MPI_COMM_WORLD,&myID);
  gpuID = 0;
  cudaSetDevice(gpuID);

  if(myID==0)
  {
    param = fopen("param.dat","r");
    fscanf(param,"%d",&N);
    fscanf(param,"%d",&Nt);
  }
  MPI_Bcast(&N ,1,MPI_INT,0,MPI_COMM_WORLD);
  MPI_Bcast(&Nt,1,MPI_INT,0,MPI_COMM_WORLD);
  dx = 2.0/(double) N;

  Np = decoupe(myID,NCPU,N,dx,&x0);
  x  = (double*)malloc((Np+2)*sizeof(double));
  prim = (double*)malloc(3*(Np+2)*sizeof(double));
  cons = (double*)malloc(3*(Np+2)*sizeof(double));
  flux = (double*)malloc(3*(Np+2)*sizeof(double));
  cudaMalloc((void**)&d_prim, 3*(Np+2)*sizeof(double));
  cudaMalloc((void**)&d_cons, 3*(Np+2)*sizeof(double));
  cudaMalloc((void**)&d_flux, 3*(Np+2)*sizeof(double));
  cudaMalloc((void**)&d_cmax, 3*(Np+2)*sizeof(double));
  cudaMalloc((void**)&d_vmax,          sizeof(double));


  init(Np,x0,dx,x,prim,cons);

  cudaMemcpy(d_prim,prim,3*(Np+2)*sizeof(double),cudaMemcpyHostToDevice);
  cudaMemcpy(d_cons,cons,3*(Np+2)*sizeof(double),cudaMemcpyHostToDevice);
  cudaMemcpy(d_flux,cons,3*(Np+2)*sizeof(double),cudaMemcpyHostToDevice);
  printf("Allocation OK\n");
  deb = clock();
  cudaEventRecord(start);
  for(it=1;it<=Nt;it++)
  {
    echange(Np,myID,NCPU,d_cons);
    VFHLLKer<<<(Np+2)/Nth+1,Nth>>>(myID, NCPU, Np, dx, d_prim, d_cons, d_flux, d_cmax, d_vmax);
    cudaDeviceSynchronize();
    MPI_Allreduce(d_vmax,d_vmax,1,MPI_DOUBLE_PRECISION,MPI_MAX,MPI_COMM_WORLD);
    VFINTKer<<<(Np+2)/Nth+1,Nth>>>(N,dx,d_cons,d_flux, d_vmax);
    cudaDeviceSynchronize();
  }
  cudaEventRecord(stop);
  cudaEventElapsedTime(&tms, start, stop);
  cudaDeviceSynchronize();
  
  fin = clock();
  Ttotal = (double) (fin - deb)/ CLOCKS_PER_SEC;
  if(myID==0)
  {
    printf("MPI %d GPU : %lf s \n",NCPU,Ttotal);
    printf("MPI %d GPU : %f s \n",NCPU,tms/1000);

  }
  cudaMemcpy(cons,d_cons,3*(Np+2)*sizeof(double),cudaMemcpyDeviceToHost);
  Hconstoprim(cons, prim, Np);
  ecrit(myID,NCPU,Np,out,x,prim);
  free(x);
  free(cons);
  free(prim);
  free(flux);
  cudaFree(d_prim);
  cudaFree(d_cons);
  cudaFree(d_cmax);
  cudaFree(d_flux);
  MPI_Finalize();
  return 0;
}
