#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>

int decoupe(int iCPU, int NCPU, int N, double dx, double *x0)
{
  int NP, reste;
  NP = N / NCPU;      
  reste = N % NCPU;  
  if(iCPU < reste)
  {  
    NP += 1; 
    *x0 = (double)(NP*iCPU)*dx;
  }
  else
  {         
    *x0 = (double)((NP+1)*reste + NP*(iCPU-reste) )*dx;
  }
  return NP+4;
}

void echange(int N, int myID, int Ncpu, double *phi)
{
  int iGauche = myID - 1;
  int iDroite = myID + 1;
  MPI_Status statut;
  int ierr;

  if(myID == 0) iGauche = Ncpu - 1; 
  if(myID == Ncpu - 1) iDroite = 0;

  if(myID%2==0) 
  { 
    
    MPI_Send(&phi[2],1,MPI_DOUBLE_PRECISION,iGauche,iGauche,MPI_COMM_WORLD);
    MPI_Send(&phi[3],1,MPI_DOUBLE_PRECISION,iGauche,1000+iGauche,MPI_COMM_WORLD);
    MPI_Recv(&phi[0],1,MPI_DOUBLE_PRECISION,iGauche,2000+myID,MPI_COMM_WORLD,&statut);
    MPI_Recv(&phi[1],1,MPI_DOUBLE_PRECISION,iGauche,3000+myID,MPI_COMM_WORLD,&statut);
    MPI_Send(&phi[N-4],1,MPI_DOUBLE_PRECISION,iDroite,2000+iDroite,MPI_COMM_WORLD);
    MPI_Send(&phi[N-3],1,MPI_DOUBLE_PRECISION,iDroite,3000+iDroite,MPI_COMM_WORLD);
    MPI_Recv(&phi[N-2],1,MPI_DOUBLE_PRECISION, iDroite,myID,MPI_COMM_WORLD,&statut);
    MPI_Recv(&phi[N-1],1,MPI_DOUBLE_PRECISION, iDroite,myID+1000,MPI_COMM_WORLD,&statut);
  }
  else 
  {   
    MPI_Recv(&phi[N-2],1,MPI_DOUBLE_PRECISION, iDroite,myID,MPI_COMM_WORLD,&statut);
    MPI_Recv(&phi[N-1],1,MPI_DOUBLE_PRECISION, iDroite,myID+1000,MPI_COMM_WORLD,&statut);
    MPI_Send(&phi[N-4],1,MPI_DOUBLE_PRECISION,iDroite,2000+iDroite,MPI_COMM_WORLD);
    MPI_Send(&phi[N-3],1,MPI_DOUBLE_PRECISION,iDroite,3000+iDroite,MPI_COMM_WORLD);
    MPI_Recv(&phi[0],1,MPI_DOUBLE_PRECISION,iGauche,2000+myID,MPI_COMM_WORLD,&statut);
    MPI_Recv(&phi[1],1,MPI_DOUBLE_PRECISION,iGauche,3000+myID,MPI_COMM_WORLD,&statut);
    MPI_Send(&phi[2],1,MPI_DOUBLE_PRECISION,iGauche,iGauche,MPI_COMM_WORLD);
    MPI_Send(&phi[3],1,MPI_DOUBLE_PRECISION,iGauche,1000+iGauche,MPI_COMM_WORLD);
  }
}

double smbKDV(int N, double dx,double a, double nu, double *phi, double *smb)
{
  int i;
  double sdx = 1./dx, dphi, d3phi,dtmax=1.0,amax=1.0;
  for(i=2;i<N-2;i++)
  {
    smb[i] = -2.0*sdx*(phi[i+1]+phi[i]+phi[i-1])*(phi[i+1]-phi[i-1]) - nu*sdx*sdx*sdx*(phi[i+2] - 2.0*phi[i+1] + 2.0*phi[i-1] - phi[i-2]);
    amax = fmax(amax,fabs(phi[i])+4.0*nu*sdx*sdx);
    printf("%d %lf \n",i,amax);
  }
  dtmax = 0.9*dx/amax;
  smb[0] = 0.0;
  smb[1] = 0.0;
  smb[N-1] = 0.0;
  smb[N-2] = 0.0;
 return dtmax;
}
void leap_frog(int N,  double dt, double *phi, double *phim, double *smb)
{
  int i;
  double phisave;
  for(i=2;i<N-2;i++)
  {
    phisave = phi[i];
    phi[i]  = phim[i] + dt*smb[i];
    phim[i] = phisave; 
  }
}

void copy(int N,double *phi1, double *phi2)
{
  int i;
  
  for(i=0;i<N;i++)
  {
    phi1[i] = phi2[i];
  }
}

void init(int N, double x0, double dx, double *x, double *phi)
{
  int i;
  for(i=0; i<N;i++)
  {
    x[i] = x0 + (double)i*dx;
    phi[i] = 0.5/(cosh(0.5*x[i])*cosh(0.5*x[i]));
  }
}

void ecrit(int myID, int NCPU, int N, double *x, double *phi,int cas)
{
  int i;
  int icpu;
  FILE* fichier;
  for(icpu=0;icpu<NCPU;icpu ++)
  {
    if(icpu==myID)
    {
      if(cas == 0)
      {
        if(myID==0)
        {
          fichier = fopen("initial.dat","w"); 
        }
        else
        {
          fichier = fopen("initial.dat","a");
        }
      }
      else
      {
        if(myID==0)
        {
          fichier = fopen("finale.dat","w"); 
        }
        else
        {
          fichier = fopen("finale.dat","a");
        }
      }
      for(i=2;i<N-2;i++)
      {
        fprintf(fichier,"%lf %lf \n",x[i],phi[i]);
      }
      fclose(fichier); // fermeture
    }
    MPI_Barrier(MPI_COMM_WORLD);
  }

}

int main(int argc, char* argv[])
{
  int N, Nt;
  double *x,*phi, *K1,*phim1,L;
  double dx,dt=0.01,dtmax,a= 1.0,nu=1.000;
  FILE* param;
  int NCPU,myID;
  int Np;
  double x0;

  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD,&NCPU);
  MPI_Comm_rank(MPI_COMM_WORLD,&myID);

  if(myID == 0)
  {
    param = fopen("param.dat","r+");
    fscanf(param,"%d",&N);
    fscanf(param,"%d",&Nt);
  }
  MPI_Bcast(&N ,1,MPI_INT,0,MPI_COMM_WORLD);
  MPI_Bcast(&Nt,1,MPI_INT,0,MPI_COMM_WORLD);
  

  L = 10.0*4.0*atan(1.0); 

  dx = L/(double) (N-1);

  Np = decoupe(myID, NCPU, N-4, dx, &x0);

  x     = malloc(Np*sizeof(double));
  phi   = malloc(Np*sizeof(double));
  K1    = malloc(Np*sizeof(double));
  phim1 = malloc(Np*sizeof(double));
 
  init(Np,-L/2.0+x0,dx,x,phi);
  ecrit(myID,NCPU,Np,x,phi,0);
  copy(Np,phim1,phi); 
  for(int i=0;i<Nt;i++)
  {
    echange(Np, myID, NCPU, phi);
    dt = smbKDV(Np,dx,a,nu,phi,K1);
    MPI_Allreduce(&dt,&dt,1,MPI_DOUBLE_PRECISION,MPI_MIN,MPI_COMM_WORLD);
    leap_frog(Np,dt,phi,phim1,K1);
  }
  ecrit(myID,NCPU,Np,x,phi,1);
  free(x);
  free(phi);
  free(K1);
  free(phim1);
  MPI_Finalize();
  
  return 0;
}
  
