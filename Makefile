
MPI_ROOT = /usr/bin/
MPI_INC = /usr/include/mpi/

CC = gcc
CPP= g++
FC = gfortran
MPICC = $(MPI_ROOT)mpicc
MPIFC = $(MPI_ROOT)mpif90
MPICPP= $(MPI_ROOT)mpicxx
NVCC  = /usr/bin/nvcc

NVLIBS = -L/lib/ -lcudart -lcuda

FLAGS = -O3 -lm
ACCFLAGS = $(FLAGS) -acc -Mcuda
NVFLAGS  = -ccbin gcc $(FLAGS) -I$(MPI_INC) -m64

.PHONY: all

all: eulermpi eulermpicuda kdvmpi kdvmpicuda

eulermpi:
	$(MPICC) $(FLAGS) eulerMPI.c -o eulerMPI 

eulermpicuda:
	$(NVCC)  $(NVFLAGS)  -o eulerMPICUDA.o -c eulerMPICUDA.cu 
	$(MPICPP) -o eulerMPICUDA eulerMPICUDA.o $(NVLIBS)
	rm eulerMPICUDA.o

kdvmpi:
	$(MPICC) $(FLAGS) kdvMPI.c -o kdvMPI

kdvmpicuda:
	$(NVCC)  $(NVFLAGS)  -o kdvMPICUDA.o -c kdvMPICUDA.cu 
	$(MPICPP) -o kdvMPICUDA kdvMPICUDA.o $(NVLIBS)
	rm kdvMPICUDA.o

clean:
	rm -f eulerMPI eulerMPICUDA kdvMPI
